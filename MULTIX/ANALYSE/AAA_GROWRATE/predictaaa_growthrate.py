# -*- coding: utf-8 -*-
"""PredictAAA_GrowthRate.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1oHWBMD9btDaTtI3JEbOYJRpxPZ-FEkcM
"""

import csv
import tensorflow.keras as keras
import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, LeakyReLU
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import train_test_split
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.metrics import mean_absolute_error
from plotly.subplots import make_subplots
from tensorflow.keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping, CSVLogger, Callback
from tensorflow.python.keras.backend import eager_learning_phase_scope
import plotly.graph_objs as go
import plotly.express as px
import plotly.figure_factory as ff
import pandas as pd
import math

root_dir = "D:/AAA/"


def readCSV(filename):
  dataset=[]
  with open(filename) as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for row in csvReader:
        dataset.append(row)
  return dataset

# define base model
def tloss(y_true,y_pred):
  sqe = tf.math.square(y_true - y_pred)
  sqe = tf.math.divide(sqe,2)
  loss = tf.math.reduce_sum(tf.math.log1p(sqe))
  return loss


def tilted_loss(q,y,f):
    e = (y-f)
    return K.mean(K.maximum(q*e, (q-1)*e), axis=-1)

def baseline_model(input_shape):
  # create model
  activation = 'relu'
  input_tensor = Input(shape=input_shape, name="AAA")
  d0 = Dropout(0.5)(input_tensor)
  l1 = Dense(256, activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d0)
  d1 = Dropout(0.5)(l1)
  l2 = Dense(128,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d1)
  d2 = Dropout(0.5)(l2)
  l3 = Dense(64,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d2)
  d3 = Dropout(0.5)(l3)
  l4 = Dense(32,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d3)
  d4 = Dropout(0.5)(l4)
  l5 = Dense(16,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d4)
  d5 = Dropout(0.5)(l5)
  l6 = Dense(8,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d5)
  d6 = Dropout(0.5)(l6)
  l7 = Dense(4,activation=activation,kernel_regularizer=keras.regularizers.l2(0.01))(d6)
  d7 = Dropout(0.5)(l7)
  outputs = Dense(1, activation='sigmoid',activity_regularizer=keras.regularizers.l2(1e-2))(d7)
  # Compile model
  # opt = SGD(learning_rate=1e-3,momentum=0.9,nesterov=False)
  opt = keras.optimizers.RMSprop(learning_rate=1e-2)
  model = keras.Model(inputs=input_tensor,outputs=outputs)
  model.compile(loss=keras.losses.MeanSquaredError(), optimizer=opt, metrics=[mean_absolute_error])
  return model

filename = root_dir + 'Train_Val_All_Features_6.csv'
# filename = root_dir + 'Training_Validation6.csv'
dataset = readCSV(filename)
filename2 = root_dir + 'Testing_All_Features_6.csv' ####### Uncomment for testing
# filename2 = root_dir + 'Testing6.csv'
test_data = readCSV(filename2) ####### Uncomment for testing

dataset = np.asarray(dataset)
print(dataset.shape)
test_data = np.asarray(test_data)
# split into input (X) and output (Y) variables
X = dataset[1:,1:]
Y = dataset[1:,0]
X = np.float64(X)
Y = np.float64(Y)

# Y[(Y<1e-3)] = 1e-6

X = np.float64(X)
Y = np.float64(Y)
print("predictors = ", X.shape)
print("targets = ", Y.shape)
num_folds = 10
# Define per-fold score containers <-- these are new
acc_per_fold = []
loss_per_fold = []

X_train = X
y_train = Y
# Normalise data
train_mean = np.mean(X_train, axis=0)
train_std = np.std(X_train, axis=0)

X_test = test_data[1:,1:]
y_test = test_data[1:,0]
X_test = np.float64(X_test)
y_test = np.float64(y_test)


tloss_item=[]
vloss_item=[]
tacc_item=[]
vacc_item=[]
unc_item=[]
pred_item = []
fold_unc = []
fold_pred = []

kF = ShuffleSplit(n_splits = num_folds,test_size=0.2)
fold_no=1
for train_idx, val_idx in kF.split(X_train):
  unc_item = []
  pred_item = []
  xtrain, xval = X_train[train_idx], X_train[val_idx]
  ytrain, yval = y_train[train_idx], y_train[val_idx]

  tmp = xtrain.shape
  input_shape = (tmp[-1],)
  # evaluate model
  model = baseline_model(input_shape)
  print(model.summary())
  early_stopping = EarlyStopping(monitor='val_loss',min_delta=1e-6,patience=200,mode='auto')
  lr_reducer = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=10, min_lr=1e-6)
  save_dir = root_dir + "AAA_growthRate_CV_full" + str(fold_no) + ".h5"
  checkpoint = ModelCheckpoint(filepath = save_dir, monitor='val_loss', verbose=1,
                              save_best_only=True, mode='min')
  callback = [lr_reducer,checkpoint]

  history = model.fit(xtrain, ytrain, batch_size=32, epochs=200,validation_data=(xval,yval),
                      callbacks=callback)

  tmpT = history.history['loss']
  tmpV = history.history['val_loss']
  accT = history.history['mean_absolute_error']
  accV = history.history['val_mean_absolute_error']
  tloss_item.append(tmpT)
  vloss_item.append(tmpV)
  tacc_item.append(accT)
  vacc_item.append(accV)


  tmplist = list(X_test)
  xtest = X_test
  ytest = y_test
  scores = model.evaluate(xtest,ytest)
  print(f'Score for fold no:{fold_no} Test loss: {scores[0]} / Test accuracy: {scores[1]}')
  if fold_no==1:
      tmp_s = scores[1]
      idx = fold_no
  else:
      ns = scores[1]
      if ns < tmp_s:
          idx = fold_no

  acc_per_fold.append(scores[1])
  loss_per_fold.append(scores[0])
  kdp = keras.Model(model.inputs,model.layers[-1].output)
  for i in range(xtest.shape[0]):
      dropout_pred = []
      for niter in range(1000):
        result = kdp(np.expand_dims(xtest[i,:],axis=0), training=True)
        dropout_pred.append(result)
      prediction = np.asarray(dropout_pred)
      pred_item.append(prediction)
  print('Fold complete..')
  fold_pred.append(pred_item)
  fold_no += 1

print(f'Average growth rate prediction test accuracy across folds: {np.mean(np.asarray(acc_per_fold))} / std: {np.std(np.asarray(acc_per_fold))}')

tloss_item = np.asarray(tloss_item)
vloss_item = np.asarray(vloss_item)
tacc_item = np.asarray(tacc_item)
vacc_item = np.asarray(vacc_item)

avg_tloss = np.mean(tloss_item,axis=0)
avg_vloss = np.mean(vloss_item,axis=0)
avg_tacc = np.mean(tacc_item,axis=0)
avg_vacc = np.mean(vacc_item,axis=0)

std_tloss = np.std(tloss_item,axis=0)
std_vloss = np.std(vloss_item,axis=0)
std_tacc = np.std(tacc_item,axis=0)
std_vacc = np.std(vacc_item,axis=0)

fold_unc = np.asarray(fold_unc)
fold_pred = np.asarray(fold_pred)



fig = go.Figure([go.Scatter(y=avg_tloss,
                            name='Training',
                            line=dict(color='rgb(0,0,200)'),mode='lines'),
                 go.Scatter(y=avg_tloss+std_tloss,mode='lines',marker=dict(color="#444"),line=dict(width=0),showlegend=False),
                 go.Scatter(y=avg_tloss-std_tloss,mode='lines', marker=dict(color="#444"), showlegend=False,line=dict(width=0),
                            fillcolor='rgba(0, 0, 100, 0.5)', fill='tonexty'),
                 go.Scatter(y=avg_vloss,
                            name='Validation',
                            line=dict(color='rgb(200,0,0)'), mode='lines'),
                 go.Scatter(y=avg_vloss + std_vloss, mode='lines', marker=dict(color="#444"), line=dict(width=0),
                            showlegend=False),
                 go.Scatter(y=avg_vloss - std_vloss, mode='lines', marker=dict(color="#444"), showlegend=False,
                            line=dict(width=0),
                            fillcolor='rgba(100, 0, 0, 0.5)', fill='tonexty')])

fig.update_layout(title='Full Feature Set', yaxis_title='Loss',xaxis_title='Epochs',
                  xaxis=dict(tickmode='linear',dtick=25),font_size=15)
# fig.show()
# fig.write_image(root_dir+"loss_full.svg")

figa = go.Figure([go.Scatter(y=avg_tacc,
                            name='Training',
                            line=dict(color='rgb(0,0,200)'),mode='lines'),
                 go.Scatter(y=avg_tacc+std_tacc,mode='lines',marker=dict(color="#444"),line=dict(width=0),showlegend=False),
                 go.Scatter(y=avg_tacc-std_tacc,mode='lines', marker=dict(color="#444"), showlegend=False,line=dict(width=0),
                            fillcolor='rgba(0, 0, 100, 0.5)', fill='tonexty'),
                 go.Scatter(y=avg_vacc,
                            name='Validation',
                            line=dict(color='rgb(200,0,0)'), mode='lines'),
                 go.Scatter(y=avg_vacc + std_vacc, mode='lines', marker=dict(color="#444"), line=dict(width=0),
                            showlegend=False),
                 go.Scatter(y=avg_vacc - std_vacc, mode='lines', marker=dict(color="#444"), showlegend=False,
                            line=dict(width=0),
                            fillcolor='rgba(100, 0, 0, 0.5)', fill='tonexty')])

figa.update_layout(title='Full Feature Set',yaxis_title='Mean Absolute Error',xaxis_title='Epochs',
                   xaxis=dict(tickmode='linear',dtick=25), font_size=15)
# figa.show()
# figa.write_image(root_dir+"acc_full.svg")

# print(np.asarray(fold_unc).shape)
print(np.asarray(fold_pred).shape)

sub_err=[]
sub_labels=[]
for i in range(xtest.shape[0]):
    tmp = np.squeeze(fold_pred[:,i,:,:])
    tmp = np.reshape(tmp,[1000*num_folds,1])
    labels = np.ones_like(tmp)*i+1
    sub_err.append(tmp)
    sub_labels.append(labels)

sub_err = np.squeeze(np.asarray(sub_err))

sub_labels = np.squeeze(np.asarray(sub_labels))
# sub_err = np.squeeze(np.reshape(sub_err,[sub_err.shape[0]*sub_err.shape[1],1]))
# sub_labels = np.squeeze(np.reshape(sub_labels,[sub_labels.shape[0]*sub_labels.shape[1],1]))

print(sub_err.shape)
print(sub_labels.shape)


fig = go.Figure()
for s in range(sub_labels.shape[0]):
    df = pd.DataFrame(dict(Uncertainty=np.squeeze(sub_err[s,:]), Sample=np.squeeze(sub_labels[s,:])))
    fig.add_trace(go.Violin(x=df['Sample'], y=df['Uncertainty'],fillcolor='seashell',line=dict(color='rgb(38,38,38)',width=0.5)))
    fig.update_traces(box_visible=True, meanline_visible=True)

fig.update_layout(title='Full Feature Set', yaxis_title='MAE: Uncertainty', xaxis_title='Sample Number',
                  xaxis=dict(tickmode='linear',dtick=1), yaxis=dict(tickmode='linear',dtick=0.01), font_size=15,showlegend=False)

# fig.write_image(root_dir+"violin_full.svg")

