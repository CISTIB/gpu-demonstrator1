## The base URL for the notebook server.
#  Leading and trailing slashes can be omitted, and will automatically be added.
#c.NotebookApp.base_url = '/MULTIX/NOTEBOOKS'

## The IP address the notebook server will listen on.
#c.NotebookApp.ip = '0.0.0.0'

#  The string should be of the form type:salt:hashed-password.
#c.NotebookApp.password = 'sha1:603cb478b41b:29ad33ea4cb8d45d05da572fcb16fb0e6df7aa86'
c.NotebookApp.password = ''

## The port the notebook server will listen on.
#c.NotebookApp.port = 8888

#c.NotebookApp.kernel_spec_manager_class = 'environment_kernels.EnvironmentKernelSpecManager'

#c.NotebookApp.iopub_data_rate_limit = 10000000000

#disable token requirement
c.NotebookApp.token = ''

#disable default webbrowser
#c.NotebookApp.browser = ''




