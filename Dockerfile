FROM nvcr.io/nvidia/tensorflow:20.01-tf1-py3

#set up environment
RUN apt-get update && apt-get install --no-install-recommends --no-install-suggests -y curl git nano unzip software-properties-common ubuntu-drivers-common
#RUN apt-add-repository ppa:graphics-drivers && apt-get update && apt-get install nvidia-driver-460 -y
RUN ubuntu-drivers autoinstall 

#installing python packages
RUN pip3 install sklearn pandas plotly psutil

#Clonning project repository
RUN cd / && git clone https://LEBRERO@bitbucket.org/CISTIB/gpu-demonstrator1.git


# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]

#Configure and deploy a Jupyter Notebook Server
EXPOSE 8888
CMD ["jupyter", "notebook", "--allow-root", "--no-browser", "--ip=0.0.0.0", "--port=8888", "--NotebookApp.token=''", "--notebook-dir=/gpu-demonstrator1/MULTIX/NOTEBOOKS"]